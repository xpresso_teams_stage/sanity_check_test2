function select_onchange() {
    if($('#sample_input').val() === 'DNN') {
        dnn_click();
    }
    if($('#sample_input').val() === 'XGBoost') {
        xgboost_click();
    }
};

function dnn_click() {
    $('#Model').val('DNN');
    $('#Store').val('238');
    $('#DayOfWeek').val('5');
    $('#Promo').val('0');
    $('#StateHoliday').val('0');
    $('#SchoolHoliday').val('0');
    $('#StoreType').val('3');
    $('#Assortment').val('2');
    $('#CompetitionDistance').val('610');
    $('#Promo2').val('0');
    $('#isCompetition').val('0');
    $('#NewAssortment').val('NA');
    $('#NewStoreType').val('NA');
    $('#sample_input').prop('selectedIndex', 0);
    $('#date').val('2014-08-02')
};

function xgboost_click() {
    $('#Model').val('XGBoost');
    $('#Store').val('NA');
    $('#DayOfWeek').val('NA');
    $('#Promo').val('0');
    $('#StateHoliday').val('NA');
    $('#SchoolHoliday').val('NA');
    $('#StoreType').val('NA');
    $('#Assortment').val('NA');
    $('#CompetitionDistance').val('610');
    $('#Promo2').val('0');
    $('#isCompetition').val('NA');
    $('#NewAssortment').val('3');
    $('#NewStoreType').val('1');
    $('#sample_input').prop('selectedIndex', 0);
    $('#date').val("");
    $('#date').disabled = true;
};

$(document).ready(function() {
	$('form').on('submit', function(event) {
        $('#output').val('')
        var model = $('#Model').val()
        var data = {
            "Model": $('#Model').val(),
            "input": {
                "Store": $('#Store').val(),
                "DayOfWeek": $('#DayOfWeek').val(),
                "Promo": $('#Promo').val(),
                "StateHoliday": $('#StateHoliday').val(),
                "SchoolHoliday": $('#SchoolHoliday').val(),
                "StoreType": $('#StoreType').val(),
                "Assortment": $('#Assortment').val(),
                "CompetitionDistance": $('#CompetitionDistance').val(),
                "Promo2": $('#Promo2').val(),
                "isCompetition": $('#isCompetition').val(),
                "NewAssortment": $('#NewAssortment').val(),
                "NewStoreType": $('#NewStoreType').val(),
                "Date": $('#date').val()
            }
         }
		$.ajax({
		    data : JSON.stringify(data),
			contentType: "application/json;charset=utf-8",
			type : 'POST',
			url : '/result'
		})
		.done(function(data) {
			if (data.error) {
				$('#output').val(data.error)
			}
			else {
			    $('#output').val('$'.concat(data.results, ' per day'))
			}
		});
		event.preventDefault();
	});
});
